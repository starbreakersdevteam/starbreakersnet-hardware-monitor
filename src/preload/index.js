import { contextBridge, ipcRenderer } from "electron";
import { electronAPI } from "@electron-toolkit/preload";
import Si from "systeminformation";

// Custom APIs for renderer
const electron = {
  ...electronAPI,
  dialog: (method, config) => ipcRenderer.invoke("dialog", method, config),
  getBuildType: () => ipcRenderer.invoke("build_type"),
};
const api = {
  ...Si,
};

// Use `contextBridge` APIs to expose Electron APIs to
// renderer only if context isolation is enabled, otherwise
// just add to the DOM global.
if (process.contextIsolated) {
  try {
    contextBridge.exposeInMainWorld("electron", electron);
    contextBridge.exposeInMainWorld("api", api);
    contextBridge.exposeInMainWorld("vite", import.meta.env);
  } catch (error) {
    console.error(error);
  }
} else {
  window.electron = electron;
  window.api = api;
  window.vite = import.meta.env;
}

if (process.platform === "win32") {
  Si.powerShellStart();
}
